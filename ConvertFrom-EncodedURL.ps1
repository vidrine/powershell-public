﻿function ConvertFrom-EncodedURL {
    <#
    .Synopsis
       Decode encoded characters in URL back to standard ASCII character set.
    
    .DESCRIPTION
       Decode encoded characters in URL back to standard ASCII character set.

       The conversion library is loaded within begin{}.  See .NOTES for the 
       source of the conversion table values.

       Author: Vidrine
       Create: 2017-11-17

    .PARAMETER URL
        Provide the encoded URL string
            
    .NOTES
        Reference:
        https://www.w3schools.com/tags/ref_urlencode.asp
        https://web.cs.dal.ca/~jamie/CS3172/examples/XHTML/entities/ASCII.html
    #>
    
    [CmdletBinding()]
    
    param (
        [Parameter(Position=0, ValueFromPipeline=$true, Mandatory=$true)]
        [String]
        $URL
    )

    begin {

        #-- Initialize encoding table within hash table
        $lkup_EncodingTable = [ordered]@{
	        '%20' = ' '
	        '%21' = '!'
	        '%22' = '"'
	        '%23' = '#'
	        '%24' = '$'
	        '%25' = '%'
	        '%26' = '&'
	        '%27' = "'"
	        '%28' = '('
	        '%29' = ')'
	        '%2a' = '*'
	        '%2b' = '+'
	        '%2c' = ','
	        '%2d' = '-'
	        '%2e' = '.'
	        '%2f' = '/'
	        '%3a' = ':'
	        '%3b' = ';'
	        '%3c' = '<'
	        '%3d' = '='
	        '%3e' = '>'
	        '%3f' = '?'
	        '%40' = '@'
	        '%5b' = '['
	        '%5c' = '\'
	        '%5d' = ']'
	        '%5e' = '^'
	        '%5f' = '_'
	        '%60' = '`'
	        '%7b' = '{'
	        '%7c' = '|'
	        '%7d' = '}'
	        '%7e' = '~'
        }
    }
    process {

        #-- Iterate through all hash keys
        forEach ( $hexCode in $lkup_EncodingTable.Keys ) {

            #-- Replace the encoded hex value (lower case) within the URL string
            $URL = $URL.Replace($hexCode,$lkup_EncodingTable[$hexcode])

            #-- Replace the encoded hex value (UPPER CASE) within the URL string
            $URL = $URL.Replace($hexCode.ToUpper(),$lkup_EncodingTable[$hexcode])
        }    
    }
    end {
    
        $URL
    }
}